﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyTowerUpgrade : MonoBehaviour
{
    GameController GameController;
    BuildManager BuildManager;
    public EnergyTowerBase towerBase;
    public GameObject UpgradeWindow;
    public GameObject Tier2Btn;
    public GameObject Tier3Btn;
    public GameObject SellBtn;
    public GameObject Tier2Indicator;
    public GameObject Tier3Indicator;
    public int currentTier;
    // Tier 2 Values
    [System.Serializable] // Makes struct contents accessable in inspector
    public struct UpgradeData
    {
        public float Damage;
        public int Range;
        public int Recharge;
        public int Cost;
        public int EnergyCost;
        public int EnergyGiven;
    };

    public UpgradeData Tier2Upgrade;
    public UpgradeData Tier3Upgrade;
    //public List<UpgradeData> UpgradeDatas;
    // Start is called before the first frame update
    void Start()
    {
        BuildManager = BuildManager.buildManagerInstance;
        GameController = GameController.gameControllerInstance;
        Tier2Btn.SetActive(true);
        Tier3Btn.SetActive(false);
        Tier2Indicator.SetActive(false);
        Tier3Indicator.SetActive(false);
        currentTier = 1;
        UpgradeWindow.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTier == 1)
        {
            SellBtn.SetActive(true);
        }

        if (currentTier == 2)
        {
            Tier2Btn.SetActive(false);
            Tier3Btn.SetActive(true);
        }

        if (currentTier == 3)
        {
            Tier3Btn.SetActive(false);
            Tier2Btn.SetActive(false);
        }
    }

    public void Tier2()
    {
        towerBase.damage = Tier2Upgrade.Damage;
        towerBase.range = Tier2Upgrade.Range;
        towerBase.rechargeTime = Tier2Upgrade.Recharge;
        towerBase.energyGiven += Tier2Upgrade.EnergyGiven;
        GameController.gold -= Tier2Upgrade.Cost;
        BuildManager.curEnergyBank += Tier2Upgrade.EnergyCost;
        BuildManager.maxEnergyBank += Tier2Upgrade.EnergyGiven;
        currentTier = 2;
        UpgradeWindow.SetActive(false);
        Tier2Indicator.SetActive(true);
    }

    public void OnMouseDown()
    {
        UpgradeWindow.SetActive(true);
    }

    public void Tier3()
    {
        towerBase.damage = Tier3Upgrade.Damage;
        towerBase.range = Tier3Upgrade.Range;
        towerBase.rechargeTime = Tier3Upgrade.Recharge;
        towerBase.energyGiven += Tier3Upgrade.EnergyGiven;
        GameController.gold -= Tier3Upgrade.Cost;
        BuildManager.curEnergyBank += Tier2Upgrade.EnergyCost;
        BuildManager.maxEnergyBank += Tier3Upgrade.EnergyGiven;
        currentTier = 3;
        UpgradeWindow.SetActive(false);
        Tier2Indicator.SetActive(false);
        Tier3Indicator.SetActive(true);
    }

    public void UpgradeUIOpen()
    {
        UpgradeWindow.SetActive(true);
    }


    public void CloseUpgradeWindow()
    {
        UpgradeWindow.SetActive(false);
    }
}
