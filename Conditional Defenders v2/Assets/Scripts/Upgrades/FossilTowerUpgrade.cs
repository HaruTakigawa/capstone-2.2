﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FossilTowerUpgrade : MonoBehaviour
{
    public FossilTowerBase towerBase;
    public GameObject UpgradeWindow;

    public GameObject SellBtn;
    BoxCollider2D ParentCollider;
    SpriteRenderer ParentRenderer;

    // Start is called before the first frame update
    void Start()
    {
        ParentRenderer = transform.parent.GetComponent<SpriteRenderer>();
        ParentCollider = transform.parent.GetComponent<BoxCollider2D>();
        SellBtn.SetActive(true);
        UpgradeWindow.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        SellBtn.SetActive(true);
        if (towerBase.maxTimeAlive <= 0)
        {
            ParentRenderer.enabled = true;
            ParentCollider.enabled = true;
        }
    }

    public void OnMouseDown()
    {
        UpgradeWindow.SetActive(true);
        SellBtn.SetActive(true);
    }

    public void UpgradeUIOpen()
    {
        UpgradeWindow.SetActive(true);
    }


    public void CloseUpgradeWindow()
    {
        UpgradeWindow.SetActive(false);
    }
}
