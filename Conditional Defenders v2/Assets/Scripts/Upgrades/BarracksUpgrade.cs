﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksUpgrade : MonoBehaviour
{
    GameController gameController;
    BuildManager buildManager;
    public BarracksBase barracks;
    public GameObject UpgradeWindow;
    public GameObject Tier2Btn;
    public GameObject Tier3Btn;
    public GameObject SellBtn;
    public GameObject Tier2Indicator;
    public GameObject Tier3Indicator;
    public int currentTier;

    [System.Serializable]
    public struct UpgradeData
    {
        public int UnitAmount;
        public int GoldCost;
        public int EnergyCost;
    };

    public UpgradeData Tier2Upgrade;
    public UpgradeData Tier3Upgrade;
    // Start is called before the first frame update
    void Start()
    {
        buildManager = BuildManager.buildManagerInstance;
        gameController = GameController.gameControllerInstance;
        Tier2Btn.SetActive(true);
        Tier3Btn.SetActive(false);
        Tier2Indicator.SetActive(false);
        Tier3Indicator.SetActive(false);
        currentTier = 1;
        UpgradeWindow.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTier == 1)
        {
            SellBtn.SetActive(true);
        }

        if (currentTier == 2)
        {
            Tier2Btn.SetActive(false);
            Tier3Btn.SetActive(true);
            Debug.Log("tier 3 up");
        }

        if (currentTier == 3)
        {
            Tier3Btn.SetActive(false);
            Tier2Btn.SetActive(false);
        }
        if (barracks.isOpened == true)
        {
            UpgradeWindow.SetActive(true);
        }

        if (barracks.isOpened == false)
        {
            CloseUpgradeWindow();
        }
    }

    public void Tier2()
    {
        gameController.gold -= Tier2Upgrade.GoldCost;
        buildManager.curEnergyBank += Tier2Upgrade.EnergyCost;
        barracks.UnitAmount = Tier2Upgrade.UnitAmount;
        currentTier = 2;
        UpgradeWindow.SetActive(false);
        Tier2Indicator.SetActive(true);
    }

    //public void OnMouseDown()
    //{
    //    UpgradeWindow.SetActive(true);
    //}

    public void Tier3()
    {
        gameController.gold -= Tier3Upgrade.GoldCost;
        buildManager.curEnergyBank += Tier2Upgrade.EnergyCost;
        barracks.UnitAmount = Tier3Upgrade.UnitAmount;
        currentTier = 3;
        UpgradeWindow.SetActive(false);
        Tier2Indicator.SetActive(false);
        Tier3Indicator.SetActive(true);
    }

    public void UpgradeUIOpen()
    {
        UpgradeWindow.SetActive(true);
    }

    public void CloseUpgradeWindow()
    {
        UpgradeWindow.SetActive(false);
        //barracks.isOpened = false;
        barracks.flagButton.SetActive(false);       
    }
}
