﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksTierUpgrade : MonoBehaviour
{
    GameController gameController;
    BuildManager buildManager;
    public BarracksUpgrade barracksUpgrade;
    public bool isTier2Upgrade;
    public bool isTier3Upgrade;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseDown()
    {
        if (isTier2Upgrade == true)
        {
            if (gameController.gold < barracksUpgrade.Tier2Upgrade.GoldCost)
            {
                Debug.Log("Not enough gold for upgrade");
            }

            else if (barracksUpgrade.Tier2Upgrade.EnergyCost + buildManager.curEnergyBank > buildManager.maxEnergyBank)
            {
                Debug.Log("Not enough energy for upgrade");
            }

            else
            {
                barracksUpgrade.Tier2();
            }
        }

        if (isTier3Upgrade == true)
        {
            if (gameController.gold < barracksUpgrade.Tier3Upgrade.GoldCost)
            {
                Debug.Log("Not enough gold for upgrade");
            }

            else if (barracksUpgrade.Tier3Upgrade.EnergyCost + buildManager.curEnergyBank > buildManager.maxEnergyBank)
            {
                Debug.Log("Not enough energy for upgrade");
            }

            else
            {
                barracksUpgrade.Tier3();
            }
        }
    }
}
