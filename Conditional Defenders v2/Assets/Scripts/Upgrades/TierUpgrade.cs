﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TierUpgrade : MonoBehaviour
{
    GameController gameController;
    BuildManager buildManager;
    public TowerUpgrade towerUpgrade;
    public bool isTier2Upgrade;
    public bool isTier3Upgrade;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
    }

    public void OnMouseDown()
    {
        if (isTier2Upgrade == true)
        {
            if (gameController.gold < towerUpgrade.Tier2Upgrade.Cost)
            {
                Debug.Log("Not enough gold for upgrade");
            }

            else if (towerUpgrade.Tier2Upgrade.EnergyCost + buildManager.curEnergyBank > buildManager.maxEnergyBank)
            {
                Debug.Log("Not enough energy for upgrade");
            }

            else
            {
                towerUpgrade.Tier2();
            }
        }

        if (isTier3Upgrade == true)
        {
            if (gameController.gold < towerUpgrade.Tier3Upgrade.Cost)
            {
                Debug.Log("Not enough gold for upgrade");
            }

            else if (towerUpgrade.Tier3Upgrade.EnergyCost + buildManager.curEnergyBank > buildManager.maxEnergyBank)
            {
                Debug.Log("Not enough energy for upgrade");
            }

            else
            {
                towerUpgrade.Tier3();
            }
        }
    }
}
