﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTile : MonoBehaviour
{
    public GameObject buildTileController;
    private GameObject turret;
    private BuildTileController tileController;
    private Color color;
    public GameObject turretPrefab;
    public GameObject UI;
    BuildManager buildManager;
    GameController gameController;
    SpriteRenderer spriteRenderer;
    private void Start()
    {
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
        tileController = buildTileController.GetComponent<BuildTileController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        color = spriteRenderer.color;
        color.a = 0.5f;
        spriteRenderer.color = color;
        UI.SetActive(false);
    }
    private void OnMouseOver()
    {
        color.a = 1;
        spriteRenderer.color = color;
        UI.SetActive(true);
    }
    private void OnMouseExit()
    {
        color.a = 0.5f;
        spriteRenderer.color = color;
        UI.SetActive(false);
    }
    void OnMouseDown()
    {
        if (turret != null)
        {
            //Sell or upgrade
        }
        if (turret == null)
        {
            buildManager.SetTurretToBuild(turretPrefab);
            GameObject turretToBuild = buildManager.GetTurretToBuild();
            //turret = (GameObject)Instantiate(turretToBuild,this.gameObject.GetComponentInParent<Transform>().transform.position,this.gameObject.GetComponentInParent<Transform>().transform.rotation);

            if (turretToBuild.GetComponent<TowerBase>().goldCost > gameController.gold)
            {
                print("Not enough gold");
                turretToBuild = null;
                return;
            }
            else if (turretToBuild.GetComponent<TowerBase>().energyRequired + buildManager.curEnergyBank > buildManager.maxEnergyBank)
            {
                print("Not enough energy");
                turretToBuild = null;
                return;
            }
            else
            {
                color.a = 0.5f;
                spriteRenderer.color = color;
                UI.SetActive(false);

                gameController.gold -= turretToBuild.GetComponent<TowerBase>().goldCost;
                buildManager.curEnergyBank += turretToBuild.GetComponent<TowerBase>().energyRequired;
                turret = (GameObject)Instantiate(turretToBuild, transform.position, transform.rotation);
                //Check for terrain and type here
                if (turret.GetComponent<EnergyTowerBase>())
                {
                    if (tileController.terrain == BuildTileController.TerrainType.Grass && !turret.GetComponent<WindTower>())
                    {
                        turret.GetComponent<EnergyTowerBase>().energyGiven = turret.GetComponent<EnergyTowerBase>().energyGiven / 2;
                    }
                    else if (tileController.terrain == BuildTileController.TerrainType.Desert && !turret.GetComponent<SolarTower>())
                    {
                        turret.GetComponent<EnergyTowerBase>().energyGiven = turret.GetComponent<EnergyTowerBase>().energyGiven / 2;
                    }
                    else if (tileController.terrain == BuildTileController.TerrainType.Volcanic && !turret.GetComponent<GeothermalTower>())
                    {
                        turret.GetComponent<EnergyTowerBase>().energyGiven = turret.GetComponent<EnergyTowerBase>().energyGiven / 2;
                    }
                }

                turret.transform.position = buildTileController.transform.position;
                turret.transform.SetParent(buildTileController.transform);
                if (turret.GetComponent<AttackTowerBase>())
                {
                    buildManager.attackTowerList.Add(turret);
                }
                else if (turret.GetComponent<BarracksBase>())
                {
                    buildManager.barracksTowerList.Add(turret);
                }
                else if (turret.GetComponent<EnergyTowerBase>())
                {
                    buildManager.energyTowerList.Add(turret);
                }
                tileController.isOpened = false;
                tileController.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                tileController.gameObject.GetComponent<BoxCollider2D>().enabled = false;                
            }
        }
    }
}

