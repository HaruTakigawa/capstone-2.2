﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTileController : MonoBehaviour
{
    ClickSoundNoButton sound { get { return GetComponent<ClickSoundNoButton>(); } }

    public enum TerrainType
    {
        Desert,
        Grass,
        Volcanic
    }
    public TerrainType terrain = TerrainType.Grass;
    public List<GameObject> buildTileList;
    public bool isOpened;
    void Start()
    {
        isOpened = false;
    }
    private void Update()
    {
        if (!isOpened)
        {
            for (int i = 0; i < buildTileList.Count; i++)
            {
                buildTileList[i].SetActive(false);
            }
        }
        else if (isOpened)
        {
            for (int i = 0; i < buildTileList.Count; i++)
            {
                buildTileList[i].SetActive(true);
            }
        }
    }
    public void OnMouseDown()
    {
        NodeManager.nodeManagerInstance.CloseOtherNodes(this.gameObject);
        if (!isOpened)
        {
            isOpened = true;
            sound.PlaySound();
        }
        else if (isOpened)
        {
            isOpened = false;
        }
    }
}
