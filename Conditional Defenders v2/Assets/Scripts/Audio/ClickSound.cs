﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class ClickSound : MonoBehaviour
{
    public AudioClip sound;

    public Button[] buttons;

    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    private void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;
        
        foreach (Button b in buttons)
        {
            b.onClick.AddListener(() => PlaySound());
        }

    }

    public void PlaySound()
    {
        source.PlayOneShot(sound);
    }
}
