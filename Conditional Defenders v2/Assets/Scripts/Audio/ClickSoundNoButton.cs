﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ClickSoundNoButton : MonoBehaviour
{
    public AudioClip sound;
    private AudioSource src { get { return GetComponent<AudioSource>(); } }

    private void Start()
    {
        gameObject.AddComponent<AudioSource>();
        src.clip = sound;
        src.playOnAwake = false;
    }

    public void PlaySound()
    {
        src.PlayOneShot(sound);
    }
}
