﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : DebuffBase
{
    UnitsBase units;
    public float damage = 5;
    float tickDamage = 1;
    void Start()
    {
        units = GetComponent<UnitsBase>();
    }
    protected override void Update()
    {
        tickDamage -= Time.deltaTime;
        if (tickDamage <= 0)
        {
            Effect();
            tickDamage = 1;
        }
        if (units == null)
        {
            Destroy(this);
        }
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            Destroy(this);
        }
    }
    public override void Effect()
    {
        units.health -= damage;
    }
}
