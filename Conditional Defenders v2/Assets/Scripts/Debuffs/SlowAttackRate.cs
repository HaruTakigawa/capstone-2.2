﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowAttackRate : DebuffBase
{
    UnitsBase unitsBase;
    private float slowAmount = 3;
    private float unitTempAttackRate;
    // Use this for initialization
    private void Start()
    {
        unitsBase = GetComponent<UnitsBase>();
        unitTempAttackRate = unitsBase.tempAttackRate;
        Effect();
    }
    protected override void Update()
    {
        if (unitsBase == null)
        {
            Destroy(this);
        }
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            unitsBase.tempAttackRate = unitTempAttackRate;
            Destroy(this);
        }
    }
    public override void Effect()
    {
        if (unitsBase != null)
            unitsBase.tempAttackRate = unitsBase.tempAttackRate + slowAmount;
    }
}
