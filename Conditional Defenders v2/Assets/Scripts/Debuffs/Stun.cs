﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun : DebuffBase
{
    UnitsBase unitsBase;
    float stunDuration = 2;
    private void Start()
    {
        unitsBase = GetComponent<UnitsBase>();
        duration = stunDuration;
        Effect();
    }
    protected override void Update()
    {
        if (unitsBase == null)
        {
            Destroy(this);
        }
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            unitsBase.unitState = UnitsBase.UnitState.Idle;
            Destroy(this);
        }
    }
    public override void Effect()
    {
        unitsBase.unitState = UnitsBase.UnitState.Stunned;
    }
}
