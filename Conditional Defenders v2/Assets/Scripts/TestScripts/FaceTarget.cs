﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceTarget : MonoBehaviour
{
    GameObject target;

    private void Start()
    {

    }
    public enum FacingDir
    {
        Right,
        Left,
        Forward,
        Backward
    }
    public FacingDir facingDir;
    private void Update()
    {
        if (target == null)
        {
            if (this.GetComponent<AttackTowerBase>().target == null) return;
            target = this.GetComponent<AttackTowerBase>().target;
        }
        else
        {
            if (this.GetComponent<AttackTowerBase>().target == null) return;
            target = this.GetComponent<AttackTowerBase>().target;
            Vector2 dir = target.transform.position - this.transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            if (angle >= -45 && angle < 45)
            {
                facingDir = FacingDir.Right;
            }
            else if (angle >= 45 && angle < 135)
            {
                facingDir = FacingDir.Forward;
            }
            else if ((angle >= 135 && angle < 180) || (angle > -180 && angle < -135))
            {
                facingDir = FacingDir.Left;
            }
            else if (angle >= -135 && angle < -45)
            {
                facingDir = FacingDir.Backward;
            }
        }
    }
}
