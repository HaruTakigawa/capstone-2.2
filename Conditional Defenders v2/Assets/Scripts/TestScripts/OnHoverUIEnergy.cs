﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OnHoverUIEnergy : MonoBehaviour
{
    public GameObject energyTowerPrefab;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI goldCostText;
    public TextMeshProUGUI energyGivenText;
    public TextMeshProUGUI lifeTimeText;
    EnergyTowerBase tower;
    private void Awake()
    {
        tower = energyTowerPrefab.GetComponent<EnergyTowerBase>();
        nameText.text = tower.towerName;
        descriptionText.text = tower.towerDescription;
        goldCostText.text = "Gold Cost: " + tower.goldCost.ToString();
        energyGivenText.text = "Energy Added: " + tower.energyGiven.ToString();
        if (tower.GetComponent<FossilTowerBase>())
        {
            lifeTimeText.text = "Duration : " + tower.GetComponent<FossilTowerBase>().maxTimeAlive.ToString();
        }
    }
}
