﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OnHoverUI : MonoBehaviour
{
    public GameObject towerPrefab;
    TowerBase tower;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI rechargeTimeText;
    public TextMeshProUGUI rangeText;
    public TextMeshProUGUI goldCost;
    public TextMeshProUGUI energyRequired;

    private void Awake()
    {
        tower = towerPrefab.GetComponent<TowerBase>();
        nameText.text = tower.towerName;
        descriptionText.text = tower.towerDescription;
        damageText.text = "Damage: " + tower.damage.ToString();
        rechargeTimeText.text = "Reload Time: " + tower.rechargeTime.ToString();
        rangeText.text = "Range: " + tower.range.ToString();
        goldCost.text = "Gold : " + tower.goldCost.ToString();
        energyRequired.text = "Energy : " + tower.energyRequired.ToString();
    }

 
}
