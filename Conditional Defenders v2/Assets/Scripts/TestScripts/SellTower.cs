﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellTower : MonoBehaviour
{
    public int sellGold;
    public GameObject tower;
    GameController gameController;
    BuildManager buildManager;
    BoxCollider2D ParentCollider;
    SpriteRenderer ParentRenderer;
    public bool isSold;
    private void Start()
    {
        ParentRenderer = transform.parent.transform.parent.transform.parent.GetComponent<SpriteRenderer>();
        ParentCollider = transform.parent.transform.parent.transform.parent.GetComponent<BoxCollider2D>();
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
        this.gameObject.SetActive(false);
    }
    private void OnMouseDown()
    {
        isSold = true;
        if (isSold == true)
        {
            ParentRenderer.enabled = true;
            ParentCollider.enabled = true;
        }

        gameController.gold += sellGold;

        if (tower.GetComponent<EnergyTowerBase>())
        {
            buildManager.maxEnergyBank -= tower.GetComponent<EnergyTowerBase>().energyGiven;
            print("Sold");
        }
        else if (tower.GetComponent<AttackTowerBase>())
        {
            buildManager.curEnergyBank -= tower.GetComponent<AttackTowerBase>().energyRequired;
        }
        else if (tower.GetComponent<BarracksBase>())
        {
            buildManager.curEnergyBank -= tower.GetComponent<BarracksBase>().energyRequired;
        }
        buildManager.attackTowerList.Remove(tower);
        Debug.Log("Is Sold");
        Destroy(tower);
    }
}
