﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class EnergyMeter : MonoBehaviour
{

    BuildManager manager;
    
    public List<GameObject> energy;
    public static EnergyMeter EnergyMeterInstance;


    public UnityEvent onEnergyAdded;
    public UnityEvent onEnergyDeducted;
    void Awake()
    {
        EnergyMeterInstance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
       manager = BuildManager.buildManagerInstance;
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.curEnergyBank != 0 && manager.maxEnergyBank != 0)
        {
            if ((manager.maxEnergyBank / 5) % 5 == 0)
            {
                AddEnergy();
            }
        }
    }

    public void AddEnergy()
    {
        print("Energy added to meter");
    }

    public void DeductEnergy()
    {
        print("Energy deducted from meter");
    }
}
