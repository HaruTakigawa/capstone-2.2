﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FossilTowerBase : EnergyTowerBase
{

    public float maxTimeAlive;
    public float pollutionGiven;
    public override void Start()
    {
        base.Start();
        GameController.gameControllerInstance.currentPollution += pollutionGiven;
    }
    public override void Update()
    {
        base.Update();
        if (maxTimeAlive <= 0)
        {
            buildManager.maxEnergyBank -= energyGiven;
            Destroy(this.gameObject);
        }
        else
        {
            maxTimeAlive -= Time.deltaTime;
        }

    }
}
