﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyTowerBase : TowerBase
{
    public int energyGiven;

    protected BuildManager buildManager;
    public virtual void Start()
    {
        buildManager = BuildManager.buildManagerInstance;
        buildManager.maxEnergyBank += energyGiven;
    }
    public virtual void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Input.mousePosition;
            // I add in a raycast if i hit nothing or i hit the ground or an enemy or another tower/build area
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(mousePosition), Vector2.zero);
            if (hit.collider == null || hit.collider.tag == "Enemy")
            {
                if (this.GetComponent<FossilTowerBase>()) return;
                this.GetComponent<EnergyTowerUpgrade>().CloseUpgradeWindow();
            }
            //print(hit.collider.name);
        }
    }
}
