﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeslaTower : AttackTowerBase
{
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
        if (target == null)
        {
            target = null;
        }
        else
        {
            float distanceToTarget = Vector2.Distance(this.transform.position, target.transform.position);
            if(distanceToTarget > range)
            {
                target = null;
            }
        }

        if (BuildManager.buildManagerInstance.curEnergyBank > BuildManager.buildManagerInstance.maxEnergyBank)
        {
            isPowered = false;
            target = null;
            isAttacking = false;
            return;
        }
        else
        {
            isPowered = true;
        }
        AddToTargets();
       
        GetTargets();
        Shoot();
    }
    public override void AddToTargets()
    {
        base.AddToTargets();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Shoot()
    {
        base.Shoot();
    }
    //public List<GameObject> targetsList;
    //public override void Start()
    //{
    //    base.Start();
    //}
    //void Update()
    //{
    //    GetTargets();
    //    Shoot();
    //}
    //public override void GetTargets()
    //{
    //    foreach(GameObject enemy in enemiesList)
    //    {
    //        float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
    //        if (distanceToEnemy < range)
    //        {
    //            if (!targetsList.Exists(ind => ind.Equals(enemy)))
    //            {
    //                targetsList.Add(enemy);
    //            }
    //        }
    //        if (distanceToEnemy > range || enemy.GetComponent<UnitsBase>().health <= 0)
    //        {
    //            targetsList.Remove(enemy);
    //        }
    //    }
    //}
    //public override void Shoot()
    //{
    //    rechargeTime -= Time.deltaTime;
    //    if (rechargeTime <= 0f) rechargeTime = 0f;
    //    if (targetsList == null) return;
    //    if (rechargeTime <= 0f)
    //    {
    //        //Fire   
    //        foreach (GameObject target in targetsList)
    //        {
    //            // damage 
    //            if (target.GetComponent<UnitsBase>().health <= 0)
    //            {
    //                SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(target);
    //            }
    //            else
    //            {
    //                GameObject bullet = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    //                bullet.GetComponent<BulletBase>().tower = this;
    //                if (bullet != null)
    //                {
    //                    bullet.GetComponent<BulletBase>().Seek(target);
    //                }
    //            }
    //        }
    //        rechargeTime = reload;
    //    }
    //}
}
