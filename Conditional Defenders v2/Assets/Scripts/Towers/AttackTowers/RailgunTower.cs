﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunTower : AttackTowerBase
{
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
        if (target == null)
        {
            target = null;
        }
        else
        {
            float distanceToTarget = Vector2.Distance(this.transform.position, target.transform.position);
            if (distanceToTarget > range)
            {
                target = null;
            }
        }
        if (BuildManager.buildManagerInstance.curEnergyBank > BuildManager.buildManagerInstance.maxEnergyBank)
        {
            isPowered = false;
            target = null;
            isAttacking = false;
            return;
        }
        else
        {
            isPowered = true;
        }
        AddToTargets();
        GetTargets();
        Shoot();
    }
    public override void AddToTargets()
    {
        base.AddToTargets();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Shoot()
    {
        base.Shoot();
    }
}
