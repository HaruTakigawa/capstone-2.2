﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFlag : MonoBehaviour
{
    public GameObject flagPrefab;
    public GameObject barracks;

    private GameObject flag;
    public bool isPlaced;
    public bool isSelectingSpot;
    private void OnMouseUp()
    {
        if (flag != null)
        {
            Destroy(flag.gameObject);
            
            isPlaced = false;
        }
        flag = Instantiate(flagPrefab, this.transform.position, this.transform.rotation);
        flag.transform.SetParent(this.transform);
        isSelectingSpot = true;
        // set the flag to the barracks flag
        this.GetComponentInParent<BarracksBase>().spawnedFlag = flag;
        //Show a different UI add the button for the rally point
    }
    private void Update()
    {
        if (flag == null) return;
        if (Input.GetMouseButton(0) && !isPlaced)
        {
            flag.transform.position = flag.transform.position;
            isPlaced = true;
            isSelectingSpot = false;
            this.GetComponentInParent<BarracksBase>().OnMouseDown();
            this.GetComponentInParent<BarracksBase>().PositionSquad(flag.transform) ;
        }     
        if (isPlaced) return;
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 dirFromMousetoBarracks = mousePosition - barracks.transform.position;
        if (dirFromMousetoBarracks.magnitude >= this.GetComponentInParent<BarracksBase>().range)
        {
            flag.transform.position = new Vector3(flag.transform.position.x,flag.transform.position.y,0);
        }
        else
        {           
            flag.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);
        }
    }
}
