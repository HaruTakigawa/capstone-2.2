﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBase : MonoBehaviour
{
    public string towerName;
    [TextArea]
    public string towerDescription;

    public float damage;
    public float range;
    public float rechargeTime;
    public bool isPowered;
    public int goldCost;
    public int energyRequired;

}
