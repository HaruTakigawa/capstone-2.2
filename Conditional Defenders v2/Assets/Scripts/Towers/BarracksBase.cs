﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksBase : TowerBase
{
    public GameObject unitPrefab;
    public int UnitAmount;
    public List<GameObject> spawnedUnits;
    private float spawnIntervalTemp;
    [Header("Rally Point")]
    public Transform rallyPoint;
    public GameObject spawnedFlag;
    public float squadRadius;
    public GameObject flagButton;
    private GameObject flag;
    public List<Vector3> squadPositionsList;
    public bool isOpened;
    public bool isSpawning;
    private bool firstSpawn;
    void SpawnUnits()
    {
        if (spawnedUnits.Count >= UnitAmount) return;
        isSpawning = true;
        GameObject unit;
        // Reached Max Limit of Units

        // Can still Spawn Units
        if (UnitAmount > spawnedUnits.Count)
        {
            squadPositionsList.Add(new Vector3(0, 0, 0));
            PositionSquad(rallyPoint);
            unit = Instantiate(unitPrefab, transform.position, transform.rotation);
            unit.transform.SetParent(rallyPoint);
            unit.GetComponent<FriendlyBase>().barracks = this.gameObject;
            spawnedUnits.Add(unit);
            SpawnerManager.spawnerManagerInstance.spawnedFriendlyList.Add(unit);
        }
    }

    public void OnMouseDown()
    {
        if (!isOpened)
        {
            flagButton.SetActive(true);
            isOpened = true;
        }
        else if (isOpened)
        {
            flagButton.SetActive(false);
            isOpened = false;
            this.GetComponent<BarracksUpgrade>().CloseUpgradeWindow();
        }
    }
    private void Start()
    {
        flagButton.SetActive(false);
        spawnIntervalTemp = rechargeTime;
        firstSpawn = true;
    }
    public virtual void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Input.mousePosition;
            // I add in a raycast if i hit nothing or i hit the ground or an enemy or another tower/build area
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(mousePosition), Vector2.zero);
            if (!this.GetComponentInChildren<MoveFlag>()) return;
            if (this.GetComponentInChildren<MoveFlag>().isSelectingSpot) return;          
            if (hit.collider == null || hit.collider.tag == "Enemy")
            {
                isOpened = false;
                this.GetComponent<BarracksUpgrade>().CloseUpgradeWindow();
            }
            //print(hit.collider.name);
        }
        if (BuildManager.buildManagerInstance.curEnergyBank > BuildManager.buildManagerInstance.maxEnergyBank)
        {
            isPowered = false;
            return;
        }
        else
        {
            isPowered = true;
        }
        //if (!isOpened)
        //{
        //    flagButton.SetActive(false);
        //}
        if (firstSpawn)
        {

            for (int i = 0; i < UnitAmount; i++)
            {
                SpawnUnits();
            }
            isSpawning = false;
            firstSpawn = false;
        }
        if (spawnedUnits.Count < UnitAmount && rechargeTime <= 0 && !firstSpawn)
        {
            rechargeTime = spawnIntervalTemp;
            SpawnUnits();
        }
        else
        {
            if (rechargeTime <= 0)
            {
                rechargeTime = 0;
                return;
            }
            else
                rechargeTime -= Time.deltaTime;
        }
        isSpawning = false;
        if (spawnedUnits.Count >= UnitAmount) PositionSquad(rallyPoint);
        if (spawnedFlag == null) return;
        flag = spawnedFlag;
        if (flag == null || !flagButton.GetComponent<MoveFlag>().isPlaced) return;
        FollowFlag();

    }
    void FollowFlag()
    {
        rallyPoint.transform.position = Vector3.MoveTowards(rallyPoint.transform.position, flag.transform.position, spawnedUnits[0].GetComponent<FriendlyBase>().movementSpeed * Time.deltaTime);
    }
    public void PositionSquad(Transform rallyPoint)
    {
        for (int i = 0; i < spawnedUnits.Count; i++)
        {
            float numberOfUnits = spawnedUnits.Count;
            float angle = i * Mathf.PI * 2f / spawnedUnits.Count;
            Vector3 newPos = new Vector3(Mathf.Cos(angle) * squadRadius, Mathf.Sin(angle) * squadRadius, 0);
            squadPositionsList[i] = (newPos + rallyPoint.position);
        }
        // i need to offset each per unit
        int counter = 0;
        foreach (GameObject unit in spawnedUnits)
        {
            unit.GetComponent<FriendlyBase>().idlePosition = squadPositionsList[counter];
            counter++;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
