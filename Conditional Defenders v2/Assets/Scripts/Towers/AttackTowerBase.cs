﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTowerBase : TowerBase
{
    public Transform firePoint;
    public float splashRadius;

    public GameObject bulletPrefab;
    public List<GameObject> enemiesList;
    protected float reload;
    public GameObject target;
    public bool isAttacking;
    public virtual void Start()
    {
        reload = rechargeTime;
    }
    public Collider2D[] publicColliders;
    public virtual void AddToTargets()
    {
        Collider2D[] enemyCollider = Physics2D.OverlapCircleAll(this.transform.position, range);
        publicColliders = enemyCollider;
        if (enemyCollider == null) return;
        foreach (Collider2D enemy in enemyCollider)
        {
            if (enemy.gameObject.GetComponent<EnemyBase>() && enemy.gameObject.GetComponent<EnemyBase>().health > 0)
            {
                if (enemiesList.Contains(enemy.gameObject))
                    return;
                else
                    enemiesList.Add(enemy.gameObject);
            }
            else
            {
                return;
            }
        }
    }
    public virtual void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Input.mousePosition;
            // I add in a raycast if i hit nothing or i hit the ground or an enemy or another tower/build area
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(mousePosition), Vector2.zero);
            if (hit.collider == null || hit.collider.tag == "Enemy")
            {
                this.GetComponent<TowerUpgrade>().CloseUpgradeWindow();
            }
            //print(hit.collider.name);
        }
    }
    //private List<GameObject> tempEnemyList;
    //public virtual void RemoveFromTargets()
    //{

    //    if (enemiesList.Count <= 0) return;
    //    foreach (GameObject enemy in enemiesList)
    //    {
    //        float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
    //        if (distanceToEnemy > range)
    //        {
    //            tempEnemyList.Add(enemy);
    //        }
    //    }
    //    //foreach (GameObject enemyTemp in tempEnemyList)
    //    //{
    //    //    enemiesList.Remove(enemyTemp);
    //    //}
    //    if (tempEnemyList.Count <= 0) return;
    //    for (int i = 0; i < tempEnemyList.Count; i++)
    //    {
    //        enemiesList.Remove(enemiesList[i]);
    //    }
    //}
    public virtual void GetTargets()
    {
        // Basic Target acquisition. 
        // Gets nearest enemy from the list
        // Then sets that to its primary target
        GameObject nearestEnemy = null;
        float shortestDistance = Mathf.Infinity;
        if (enemiesList.Count <= 0) return;
        foreach (GameObject enemy in enemiesList)
        {
            float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy;
        }
        else
        {
            target = null;
        }
    }
    public virtual void Shoot()
    {
        rechargeTime -= Time.deltaTime;
        if (rechargeTime <= 0f) rechargeTime = 0f;
        if (target == null) return;

        if (rechargeTime <= 0f)
        {
            //Fire
            isAttacking = true;
            GameObject bullet = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            bullet.GetComponent<BulletBase>().tower = this;
            if (bullet != null)
            {
                bullet.GetComponent<BulletBase>().Seek(target);
            }
            rechargeTime = reload;
            print("Shoot");
        }
        else
        {
            isAttacking = false;
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
