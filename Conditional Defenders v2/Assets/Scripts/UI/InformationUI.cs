﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InformationUI : MonoBehaviour
{   
    public Text nameText;
    public Text healthText;
    public Text goldDropText;
    public GameObject InfoUI;
    public string objectName;
    public string healthAmount;
    public string goldDropAmount;
    // Start is called before the first frame update
    void Start()
    {
        InfoUI.SetActive(false);
        nameText.text = "";
        healthText.text = "";
        goldDropText.text = "";
    }


    void OnMouseOver()
    {
        nameText.text = "Name: " + objectName;
        healthText.text = "Health: " + healthAmount;
        goldDropText.text = "Gold Dropped: " + goldDropAmount;
        InfoUI.SetActive(true);
    }

    void OnMouseExit()
    {
        nameText.text = "";
        healthText.text = "";
        goldDropText.text = "";
        InfoUI.SetActive(false);
    }
}
