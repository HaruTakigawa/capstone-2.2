﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PauseFunction : MonoBehaviour
{
    public GameObject pauseBtn;
    public GameObject PauseMenu;
    public AudioSource AudioSrc;

    // Start is called before the first frame update
    void Start()
    {
        pauseBtn.SetActive(true);
        PauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Pause()
    {
        PauseAudio();
        Time.timeScale = 0.0f;
        pauseBtn.SetActive(false);
        PauseMenu.SetActive(true);
    }

    public void Resume()
    {
        UnpauseAudio();
        Time.timeScale = 1.0f;
        pauseBtn.SetActive(true);
        PauseMenu.SetActive(false);
    }

    public void MainMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Main Menu");
    }

    void PauseAudio()
    {
        AudioSrc.Pause();
    }

    void UnpauseAudio()
    {
        AudioSrc.UnPause();
    }
}
