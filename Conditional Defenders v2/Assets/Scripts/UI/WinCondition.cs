﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class WinCondition : MonoBehaviour
{
    private CoalBossEnemy boss;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        boss = GameObject.Find("CoalBossUnit").GetComponent<CoalBossEnemy>();
        if (boss.health <= 0)
        {
            SceneManager.LoadScene("WinScene");
        }
    }
}
