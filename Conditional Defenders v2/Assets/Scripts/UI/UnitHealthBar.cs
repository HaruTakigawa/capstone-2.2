﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UnitHealthBar : MonoBehaviour
{
    public Image HealthBar;
    public UnitsBase Unit;
    private float startHealth;
    // Start is called before the first frame update
    void Start()
    {
       startHealth = Unit.health;
    }

    // Update is called once per frame
    void Update()
    {
        HealthBar.fillAmount = Unit.health / startHealth;
        //Debug.Log(Unit.health);
    }
}
