﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class TutorialUI : MonoBehaviour
{
    public GameObject TutorialWindow;
    public List<GameObject> Sets;
    public TextMeshProUGUI NextText;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0.0f;
        TutorialWindow.SetActive(true);
        Sets[0].SetActive(true);

    }
    
    public void NextBtn()
    {
        if (Sets[0].activeSelf)
        {
            Sets[1].SetActive(true);
            Sets[0].SetActive(false);
        }

        else if ((Sets[1].activeSelf))
        {
            Sets[2].SetActive(true);
            Sets[1].SetActive(false);
            NextText.text = "Finish";
        }

        else if ((Sets[2].activeSelf))
        {
            TutorialWindow.SetActive(false);
            Time.timeScale = 1.0f;
        }
    }

    public void SkipBtn()
    {
        TutorialWindow.SetActive(false);
        Time.timeScale = 1.0f;
    }
}
