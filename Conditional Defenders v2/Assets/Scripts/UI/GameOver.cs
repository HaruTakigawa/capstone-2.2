﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour
{
    public GameController controller;

    // Update is called once per frame
    void Update()
    {
        if(controller.currentPollution >= controller.maxPollution)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
