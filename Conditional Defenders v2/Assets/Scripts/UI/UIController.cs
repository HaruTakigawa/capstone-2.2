﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIController : MonoBehaviour
{
    public Text maxEnergy;
    public Text curEnergy;
    public Text currentGold;
    public Text waveNumber;
    public Image pollutionBar;
    public Image energyBar;
    public GameController gameController;
    public BuildManager buildManager;
    public SpawnerManager spawnerManager;

    // Start is called before the first frame update
    void Start()
    {
        gameController = GameController.gameControllerInstance;
        buildManager = BuildManager.buildManagerInstance;
        spawnerManager = SpawnerManager.spawnerManagerInstance;
    }

    // Update is called once per frame
    void Update()
    {
        currentGold.text = gameController.gold.ToString() + " ";
        maxEnergy.text = "/ " + buildManager.maxEnergyBank.ToString();
        curEnergy.text = buildManager.curEnergyBank.ToString();
        waveNumber.text = spawnerManager.currentWave.ToString();
        pollutionBar.fillAmount = gameController.currentPollution / gameController.maxPollution;
        energyBar.fillAmount = buildManager.curEnergyBank / buildManager.maxEnergyBank;
    }
}
