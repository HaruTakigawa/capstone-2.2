﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TeslaBullet : BulletBase
{
    public float bounceAmount;
    public float bounceRange;
    public List<GameObject> damagedTargets;
    public List<GameObject> enemiesList;
    float curBounceAmount;
    private void Start()
    {
        curBounceAmount = 0;
    }

    public override void Update()
    {
        AddToTargets();
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        Vector2 dir = target.transform.position - transform.position;
        float distanceThisFrame = bulletSpeed * Time.deltaTime;
        if (dir.magnitude <= distanceThisFrame)
        {
            if (curBounceAmount >= bounceAmount)
            {
                Destroy(this.gameObject);
            }

            HitTarget();
            print("hit");
            return;

        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }
    public Collider2D[] publicColliders;
    void AddToTargets()
    {
        Collider2D[] enemyCollider = Physics2D.OverlapCircleAll(this.transform.position, bounceRange * 2);
        publicColliders = enemyCollider;
        if (enemyCollider == null) return;
        foreach(Collider2D enemy in enemyCollider)
        {
            if (enemy.gameObject.GetComponent<EnemyBase>())
            {
                if (enemiesList.Contains(enemy.gameObject))
                    return;
                else
                    enemiesList.Add(enemy.gameObject);
            }
            else
            {
                return;
            }          
        }
    }
    void FindNextTarget()
    {
        GameObject nearestEnemy = null;
        float shortestDistance = Mathf.Infinity;
        if (enemiesList.Count <= 0) return;

        foreach (GameObject enemy in enemiesList)
        {
            if (enemy != null)
            {
                float distanceToEnemy = Vector2.Distance(this.transform.position, enemy.transform.position);

                if (!damagedTargets.Contains(enemy))
                {
                    if (distanceToEnemy < shortestDistance)
                    {
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = enemy;
                    }
                }
            }                      
        }
        if (nearestEnemy != null && shortestDistance <= bounceRange)
        {
            target = nearestEnemy;
        }
        else
        {
            target = null;
        }
    }
    public override void HitTarget()
    {
        damagedTargets.Add(target);
        DamageEnemy(target);
        curBounceAmount++;
        FindNextTarget();
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, bounceRange);
    }
}