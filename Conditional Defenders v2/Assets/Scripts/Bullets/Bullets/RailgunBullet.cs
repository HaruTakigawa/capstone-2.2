﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunBullet : BulletBase
{
    public float flightTime;
    public List<GameObject> colliders;
    Vector3 dir;
    private void Start()
    {
        dir = (target.transform.position - transform.position).normalized;
        Quaternion newAngle = Quaternion.LookRotation((target.transform.position - this.transform.position));
        this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, newAngle.z, 1);
    }
    public override void Update()
    {     
        float distanceThisFrame = bulletSpeed * Time.deltaTime;
        //transform.position = Vector2.MoveTowards(transform.position, target.transform.position, distanceThisFrame);
        //HitTarget();
        transform.position += dir * distanceThisFrame;
        flightTime -= Time.deltaTime;
        if (flightTime <= 0)
        {
            flightTime = 0;
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Change it to enemy Base;
        if (collision.gameObject.GetComponent<EnemyBase>())
        {
            DamageEnemy(collision.gameObject);
            //colliders.Add(collision.gameObject);
        }
    }
}
