﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBase : MonoBehaviour
{
    public AttackTowerBase tower;
    public float bulletSpeed;
    protected GameObject target;
    public void Seek(GameObject _target)
    {
        target = _target;
    }
    public virtual void DamageEnemy(GameObject enemy)
    {
        enemy.GetComponent<UnitsBase>().TakeDamage(tower.damage);
    }
    public virtual void HitTarget()
    {
        DamageEnemy(target);
        Destroy(this.gameObject);
    }
    public virtual void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector2 dir = target.transform.position - transform.position;
        float distanceThisFrame = bulletSpeed * Time.deltaTime;
        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            print("hit");
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }
}
