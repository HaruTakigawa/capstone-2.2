﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    GameController gameController;
    public List<GameObject> spawnedEnemiesList;
    public List<GameObject> spawnedFriendlyList;

    public Transform spawner;
    public static SpawnerManager spawnerManagerInstance;

    public enum State
    {
        Spawning,
        Waiting,
        Counting
    }
    [System.Serializable]
    public class WaveList
    {
        public List<GameObject> monsterList;
    }
    public int currentWave;
    public State state;

    public float timeTillNextWave;

    public List<WaveList> waveList;
    private float defaultTimeForWave;

    private float spawnDelay = 2;
    void Awake()
    {
        if (spawnerManagerInstance != null)
        {
            print("More than 1 Spawner Manager Instance");
        }
        else
            spawnerManagerInstance = this;
    }

    IEnumerator SpawnIndividual(WaveList waveList, float spawnDelay)
    {
        state = State.Spawning;
        for (int i = 0; i < waveList.monsterList.Count; i++)
        {
            SpawnEnemy(waveList.monsterList[i]);
            yield return new WaitForSeconds(spawnDelay);
        }
        state = State.Waiting;
        yield break;
    }
    void SpawnEnemy(GameObject enemy)
    {
        GameObject monster;
        monster = Instantiate(enemy, spawner.position, spawner.rotation);
        BuffEnemy(monster);
        monster.transform.SetParent(spawner);
        spawnedEnemiesList.Add(monster);
    }
    //IEnumerator Spawn(WaveList waveList)
    //{
    //    GameObject monster;
    //    state = State.Spawning;
    //    for (int i = 0; i < waveList.monsterList.Count; i++)
    //    {
    //        monster = Instantiate(waveList.monsterList[i], spawner.position, spawner.rotation);
    //        spawnedEnemiesList.Add(monster);
    //    }
    //    state = State.Waiting;
    //    yield return new WaitForSeconds(1.0f);
    //}
    private void Start()
    {
        gameController = GameController.gameControllerInstance;
        defaultTimeForWave = timeTillNextWave;
    }
    private bool firstWave = true;
    private void Update()
    {
        if (currentWave == 0 && firstWave)
        {
            firstWave = false;
            timeTillNextWave = 2;
        }
        if (AreEnemiesAllDead() && state == State.Waiting)
        {
            timeTillNextWave = 2;
            state = State.Counting;
        }
        if (timeTillNextWave <= 0)
        {
            state = State.Counting;
            if (state != State.Spawning)
            {
                if (currentWave >= waveList.Count) return;
                StartCoroutine(SpawnIndividual(waveList[currentWave], spawnDelay));
                //StartCoroutine(Spawn(waveList[wave]));
                timeTillNextWave = defaultTimeForWave;
                currentWave++;
            }
        }
        else
        {
            timeTillNextWave -= Time.deltaTime;
        }

    }
    public void BuffEnemy(GameObject enemyUnit)
    {
        float normalizePollution = gameController.currentPollution / gameController.maxPollution;
        UnitsBase enemy = enemyUnit.GetComponent<UnitsBase>();
        enemy.health += (enemy.health * normalizePollution);
        enemy.damage += (enemy.damage * normalizePollution);
        enemy.movementSpeed += (enemy.movementSpeed * normalizePollution);
        enemy.attackRate += (enemy.attackRate * normalizePollution);
    }
    bool AreEnemiesAllDead()
    {
        foreach (GameObject enemy in spawnedEnemiesList)
        {
            if (enemy.GetComponent<UnitsBase>().health >= 0)
            {
                return false;
            }
        }
        return true;
    }
}
