﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public List<GameObject> nodeList;
    public static NodeManager nodeManagerInstance;
    void Awake()
    {
        if (nodeManagerInstance != null)
        {
            print("More than 1 build manager instance");
        }
        nodeManagerInstance = this;
    }
    public void CloseOtherNodes(GameObject myNode)
    {
        foreach (GameObject node in nodeList)
        {
            if (myNode != node)
            {
                node.GetComponent<BuildTileController>().isOpened = false;
            }
        }
    }
}
