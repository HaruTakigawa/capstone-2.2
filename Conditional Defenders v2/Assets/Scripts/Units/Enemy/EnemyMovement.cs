﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    UnitsBase unitsBase;
    public Transform target;
    public int wavepointIndex;
    // Start is called before the first frame update
    void Start()
    {        
        unitsBase = this.GetComponent<UnitsBase>();
        if (unitsBase.unitName != "bossSpawn")
        {
            target = Waypoints.points[0];
            wavepointIndex = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(unitsBase.unitState == UnitsBase.UnitState.Attacking || unitsBase.unitState == UnitsBase.UnitState.Stunned)
        {
            return;
        }
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * unitsBase.movementSpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            GameController.gameControllerInstance.currentPollution += GameController.gameControllerInstance.addedPollution;
            SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Remove(this.gameObject);
            foreach (GameObject tower in BuildManager.buildManagerInstance.attackTowerList)
            {
                tower.GetComponent<AttackTowerBase>().enemiesList.Remove(this.gameObject);
            }
            Destroy(gameObject);
            return;
        }

        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
    }
}
