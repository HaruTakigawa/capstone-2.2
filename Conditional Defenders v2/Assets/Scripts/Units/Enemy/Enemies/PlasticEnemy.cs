﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasticEnemy : EnemyBase
{
    public float chanceToStun;
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Attack()
    {
        if (attackRate <= 0)
        {
            target.GetComponent<UnitsBase>().TakeDamage(damage);
            attackRate = tempAttackRate;
            float randValue = Random.Range(1, 100);
            if (randValue < chanceToStun)
            {
                target.gameObject.AddComponent<Stun>();
            }
        }
    }
    public override void GoToTarget()
    {
        base.GoToTarget();
    }
}
