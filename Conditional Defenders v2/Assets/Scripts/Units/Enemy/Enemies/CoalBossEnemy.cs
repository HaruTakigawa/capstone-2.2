﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoalBossEnemy : EnemyBase
{
    public GameObject coalPrefab;
    public float spawnDelay;
    private float tempSpawnDelay;
    public override void Start()
    {
        base.Start();
        tempSpawnDelay = spawnDelay;
        //StartCoroutine(SpawnIndividual(spawnDelay));
    }
    public override void Update()
    {
        base.Update();
        if (spawnDelay <= 0)
        {
            spawnDelay = tempSpawnDelay;
            SpawnCoalMinions(coalPrefab);

        }
        else
        {
            spawnDelay -= Time.deltaTime;
        }
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void GoToTarget()
    {
        base.GoToTarget();
    }
    IEnumerator SpawnIndividual(float spawnDelay)
    {
        SpawnCoalMinions(coalPrefab);
        yield return new WaitForSeconds(spawnDelay);
        yield break;
    }
    void SpawnCoalMinions(GameObject enemy)
    {
        GameObject monster;
        monster = Instantiate(enemy, this.transform.position, this.transform.rotation);
        monster.GetComponent<EnemyBase>().unitName = "bossSpawn";
        monster.GetComponent<EnemyBase>().unitState = UnitState.Moving;
        monster.GetComponent<EnemyMovement>().target = this.gameObject.GetComponent<EnemyMovement>().target;
        monster.GetComponent<EnemyMovement>().wavepointIndex = this.gameObject.GetComponent<EnemyMovement>().wavepointIndex;
        SpawnerManager.spawnerManagerInstance.spawnedEnemiesList.Add(monster);
    }
}
