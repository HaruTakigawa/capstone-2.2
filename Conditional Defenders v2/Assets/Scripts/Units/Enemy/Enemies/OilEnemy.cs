﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilEnemy : EnemyBase
{
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Attack()
    {
        if (attackRate <= 0)
        {
            if (target.GetComponent<SlowAttackRate>() == null)
                target.AddComponent<SlowAttackRate>();
            else
                target.GetComponent<SlowAttackRate>().timer = 0;
        }
        base.Attack();
    }
    public override void GoToTarget()
    {
        base.GoToTarget();
    }
}
