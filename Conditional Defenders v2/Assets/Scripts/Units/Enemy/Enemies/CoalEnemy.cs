﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoalEnemy : EnemyBase
{
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void GoToTarget()
    {
        base.GoToTarget();
    }
}
