﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasEnemy : EnemyBase
{
    public float poisonAoe;
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
        Collider2D[] unitsToPoison = Physics2D.OverlapCircleAll(this.transform.position, poisonAoe);
        if (unitsToPoison == null) return;
        foreach (Collider2D opponent in unitsToPoison)
        {
            if (opponent.GetComponent<EnemyBase>() != null) return;
            if (opponent.gameObject.GetComponent<Poison>() == null)
            {
                opponent.gameObject.AddComponent<Poison>();
            }
            else
            {
                opponent.gameObject.GetComponent<Poison>().timer = 0;
            }
        }
    }
    public override void GetTargets()
    {
        base.GetTargets();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void GoToTarget()
    {
        base.GoToTarget();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, poisonAoe);
    }
}
