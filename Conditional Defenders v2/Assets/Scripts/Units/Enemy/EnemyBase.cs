﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : UnitsBase
{
    public int goldAmount;
    public override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        if (target == null)
        {
            target = null;
            unitState = UnitState.Moving;
            isEngaged = false;
        }
        if (target != null)
        {
            if (target.GetComponent<FriendlyBase>().health <= 0)
            {
                target = null;
            }
                unitState = UnitState.Attacking;
        }
        if (health <= 0)
        {
            foreach (GameObject friendlyUnit in spawnerManager.spawnedFriendlyList)
            {
                friendlyUnit.GetComponent<FriendlyBase>().targetList.Remove(this.gameObject);
            }
            foreach (GameObject tower in buildManager.attackTowerList)
            {
                tower.GetComponent<AttackTowerBase>().enemiesList.Remove(this.gameObject);
            }
            spawnerManager.spawnedEnemiesList.Remove(this.gameObject);
            gameController.gold += goldAmount;
            Destroy(this.gameObject);
        }
        AddToTargets();
        GetTargets();
        GoToTarget();
        base.Update();
    }
    public override void AddToTargets()
    {
        Collider2D[] opponentCollider = Physics2D.OverlapCircleAll(this.transform.position, detectionRange);
        if (opponentCollider == null) return;
        foreach (Collider2D opponent in opponentCollider)
        {
            if (opponent.gameObject.GetComponent<FriendlyBase>())
            {
                if (!targetList.Contains(opponent.gameObject))
                {
                    targetList.Add(opponent.gameObject);
                }
                else
                    return;
            }
        }
    }
    public override void GetTargets()
    {
        if (target == null)
        {
            target = null;
            unitState = UnitState.Moving;
            isEngaged = false;
        }
        base.GetTargets();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void GoToTarget()
    {
        if (target == null)
        {
            unitState = UnitState.Moving;
            isEngaged = false;
            return;
        }
        else
        {
            //base.GoToTarget(); 
            unitState = UnitState.Attacking;
            Attack();

            //Vector2 dir = target.transform.position - transform.position;
            ////Vector2 dir = (target.transform.position + new Vector3((attackStateOffset.x * direction), attackStateOffset.y, attackStateOffset.z)) - transform.position;

            //float distanceThisFrame = movementSpeed * Time.deltaTime;
            //if (dir.magnitude <= distanceThisFrame)
            //{
            //    print("Attacking");
            //    Attack();
            //}
        }
    }
}
