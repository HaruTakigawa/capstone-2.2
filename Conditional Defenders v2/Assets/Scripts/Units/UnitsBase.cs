﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsBase : MonoBehaviour
{
    public enum UnitState
    {
        Attacking,
        Moving,
        Idle,
        Stunned
    }
    protected GameController gameController;
    protected SpawnerManager spawnerManager;
    protected BuildManager buildManager;
    public GameObject target;
    [Header("Unit Data")]
    public string unitName;
    public UnitState unitState;
    public float health;
    public float damage;
    public float attackRate;
    public float movementSpeed;
    public float detectionRange;
    public Vector3 attackStateOffset;
    public List<GameObject> targetList;
    public bool isEngaged;
    [HideInInspector]
    public float tempAttackRate;
    public bool isAtEnemy;
    [HideInInspector]
    public float direction;
    public void TakeDamage(float damage)
    {
        health -= damage;
    }
    public virtual void GoToTarget()
    {
        Vector2 dir = (target.transform.position + new Vector3((attackStateOffset.x * direction), attackStateOffset.y, attackStateOffset.z)) - transform.position;

        float distanceThisFrame = movementSpeed * Time.deltaTime;
        if (dir.magnitude <= distanceThisFrame)
        {
            Attack();
            isAtEnemy = true;
        }
        else
        {
            transform.Translate(dir.normalized * distanceThisFrame, Space.World);
            isAtEnemy = false;
        }

        //transform.LookAt(target.transform);
    }
    public virtual void GetTargets()
    {
        if (target != null) return;
        GameObject nearestOpponent = null;
        float shortestDistance = Mathf.Infinity;
        if (targetList.Count <= 0) return;
        foreach (GameObject opponent in targetList)
        {
            float distanceToOpponent = Vector2.Distance(this.transform.position, opponent.transform.position);
            if (this.transform.position.x > opponent.transform.position.x)
            {
                direction = 1.0f;
            }
            else
            {
                direction = -1.0f;
            }
            if (distanceToOpponent < shortestDistance)
            {
                shortestDistance = distanceToOpponent;
                if (!opponent.GetComponent<UnitsBase>().isEngaged) nearestOpponent = opponent;

            }
            if (nearestOpponent != null && shortestDistance <= detectionRange)
            {
                target = nearestOpponent;

                unitState = UnitState.Attacking;
                target.GetComponent<UnitsBase>().target = this.gameObject;
                target.GetComponent<UnitsBase>().isEngaged = true;
                //target.GetComponent<UnitsBase>().unitState = UnitState.Attacking;
                isEngaged = true;
            }
            else
            {
                target = null;
            }
        }
    }
    public virtual void AddToTargets()
    {

    }
    public virtual void Start()
    {
        tempAttackRate = attackRate;
        gameController = GameController.gameControllerInstance;
        spawnerManager = SpawnerManager.spawnerManagerInstance;
        buildManager = BuildManager.buildManagerInstance;
    }

    public virtual void Update()
    {
        if (attackRate >= 0 && unitState != UnitState.Stunned)
        {
            attackRate -= Time.deltaTime;
        }
    }
    public virtual void Attack()
    {
        if (attackRate <= 0)
        {
            target.GetComponent<UnitsBase>().TakeDamage(damage);
            attackRate = tempAttackRate;
        }
        else
        {
            return;
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}
