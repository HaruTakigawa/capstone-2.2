﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyBase : UnitsBase
{
    public GameObject publicTarget;
    public Vector3 idlePosition;
    public GameObject barracks;
    private float tempAttackRateFriendly;
    public override void Start()
    {
        unitState = UnitState.Idle;
        tempAttackRateFriendly = attackRate;
        idlePosition = this.transform.position;
        base.Start();

    }
    public override void Update()
    {
        if (target == null)
        {
            target = null;
            unitState = UnitState.Idle;
            isEngaged = false;
        }
        if (unitState == UnitState.Stunned)
        {
            return;
        }
        if (unitState == UnitState.Idle)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, idlePosition, movementSpeed * Time.deltaTime);
        }
        publicTarget = target;
        if (target != null)
        {
            unitState = UnitState.Attacking;
        }
        else
        {
            unitState = UnitState.Idle;
        }
        if (health <= 0)
        {
            barracks.GetComponent<BarracksBase>().spawnedUnits.Remove(this.gameObject);
            foreach (GameObject enemyUnit in spawnerManager.spawnedEnemiesList)
            {
                enemyUnit.GetComponent<EnemyBase>().targetList.Remove(this.gameObject);
            }
            spawnerManager.spawnedFriendlyList.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
        AddToTargets();
        GetTargets();
        GoToTarget();
        base.Update();
    }
    public override void AddToTargets()
    {
        Collider2D[] opponentColliders = Physics2D.OverlapCircleAll(this.transform.position, detectionRange);
        if (opponentColliders == null) return;
        foreach (Collider2D opponent in opponentColliders)
        {
            if (opponent.gameObject.GetComponent<EnemyBase>())
            {
                if (!targetList.Contains(opponent.gameObject))
                {
                    targetList.Add(opponent.gameObject);
                }
                else
                    return;
            }
        }
    }
    public override void GetTargets()
    {
        /*
        // If target is engaged with another unit dont attack unless there is no other unit
        // nearby
        if (target != null) return;
        GameObject nearestOpponent = null;
        float shortestDistance = Mathf.Infinity;
        if (targetList.Count <= 0) return;
        foreach (GameObject opponent in targetList)
        {
            float distanceToOpponent = Vector2.Distance(this.transform.position, opponent.transform.position);
            if (this.transform.position.x > opponent.transform.position.x)
            {
                direction = 1.0f;
            }
            else
            {
                direction = -1.0f;
            }
            if (distanceToOpponent < shortestDistance)
            {
                shortestDistance = distanceToOpponent;
                // Wala syang kalaban 
                // so i check if there are other nearby opponents
                // if wala then i set that enemy as my opponent as well
                // if meron then go to a different enemy
                if (!opponent.GetComponent<UnitsBase>().isEngaged)
                {
                    print("Non Engaged Enemy Found");
                    nearestOpponent = opponent;
                }
                else if (opponent.GetComponent<UnitsBase>().isEngaged && targetList.Count == 1)
                {
                    print("Engaged opponent found and 1 opponent only");
                    nearestOpponent = opponent;                    
                }
                else
                {
                    //GetTargets();
                    target = null;
                    print("More than 1 enemy");
                }

            }
            if (nearestOpponent != null && shortestDistance <= detectionRange)
            {
                target = nearestOpponent;

                unitState = UnitState.Attacking;
                target.GetComponent<UnitsBase>().target = this.gameObject;
                target.GetComponent<UnitsBase>().isEngaged = true;
                //target.GetComponent<UnitsBase>().unitState = UnitState.Attacking;
                isEngaged = true;
            }
            else
            {
                target = null;
            }
        }*/
        if (target == null)
        {
            target = null;
            unitState = UnitState.Idle;
            isEngaged = false;
        }
        base.GetTargets();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void GoToTarget()
    {
        if (target == null)
        {
            unitState = UnitState.Idle;
            isEngaged = false;
            return;
        }
        else
            base.GoToTarget();
    }
}
