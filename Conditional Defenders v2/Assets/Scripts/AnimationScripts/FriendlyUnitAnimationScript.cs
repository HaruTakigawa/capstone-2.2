﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyUnitAnimationScript : MonoBehaviour
{
    Animator anim;
    public UnitsBase Base;
    public FriendlyBase FBase;
    public SpriteRenderer Renderer;
    [HideInInspector]
    public float CurrentXPosition;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        CurrentXPosition = this.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {

        if (this.transform.position.x > CurrentXPosition)
        {
            Renderer.flipX = false;
            CurrentXPosition = this.transform.position.x;
            anim.SetBool("IsWalking", true);
        }

        if (this.transform.position.x < CurrentXPosition)
        {
            Renderer.flipX = true;
            CurrentXPosition = this.transform.position.x;
            anim.SetBool("IsWalking", true);
        }

        if (Base.isEngaged == true && Base.isAtEnemy == false)
        {
            anim.SetBool("IsWalking", true);
        }

        if (Base.isEngaged == true && Base.isAtEnemy == true)
        {
            anim.SetBool("IsAttacking", true);
        }

        if (Base.isEngaged == false)
        {
            anim.SetBool("IsAttacking", false);
            anim.SetBool("IsWalking", true);

            if (this.transform.position == FBase.idlePosition && FBase.publicTarget == null)
            {
                anim.SetBool("IsWalking", false);
            }
        }
    }
}
