﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerAnimationScript : MonoBehaviour
{
    GameController controller;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        controller = GameController.gameControllerInstance;
        anim = GetComponent<Animator>();
        anim.SetBool("IsAtForm2", false);
        anim.SetBool("IsAtForm3", false);
        anim.SetBool("IsAtForm4", false);
    }

    // Update is called once per frame
    void Update()
    {
        SwitchAnimation();
    }

    void SwitchAnimation()
    {
        if (controller.currentPollution >= 25)
        {
            anim.SetBool("IsAtForm1", true);
        }

        if (controller.currentPollution >= 50)
        {
            anim.SetBool("IsAtForm2", true);
            anim.SetBool("IsAtForm1", false);
        }

        if (controller.currentPollution >= 75)
        {
            anim.SetBool("IsAtForm3", true);
            anim.SetBool("IsAtForm2", false);
        }

        if (controller.currentPollution >= 100)
        {
            anim.SetBool("IsAtForm4", true);
            anim.SetBool("IsAtForm3", false);
        }
    }
}
