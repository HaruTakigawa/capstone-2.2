﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksAnimationScript : MonoBehaviour
{
    Animator anim;
    public BarracksBase barracks;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (barracks.isSpawning == true)
        {
            anim.SetBool("isSpawning", true);
        }

        else
        {
            anim.SetBool("isSpawning", false);
        }
    }
}
