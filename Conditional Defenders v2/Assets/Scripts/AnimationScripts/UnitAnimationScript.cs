﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimationScript : MonoBehaviour
{
    Animator anim;
    public UnitsBase Base;
    public SpriteRenderer Renderer;
    [HideInInspector]
    public float CurrentXPosition;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        //anim.SetBool("IsWalking", true);
        CurrentXPosition = this.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (this.transform.position.x > CurrentXPosition)
        {
            Renderer.flipX = true;
            CurrentXPosition = this.transform.position.x;
            anim.SetBool("IsWalking", true);
            //Debug.Log("Facing Right");
        }

        if (this.transform.position.x < CurrentXPosition)
        {
            Renderer.flipX = false;
            CurrentXPosition = this.transform.position.x;
            anim.SetBool("IsWalking", true);
            //Debug.Log("Facing Left");
        }
        //Renderer.flipX = Base.direction == -1f;
        //Debug.Log(Renderer.flipX);
        //Debug.Log(Base.direction);

        if (Base.isEngaged == true)
        {
            //anim.SetBool("IsWalking", false);
            anim.SetBool("IsAttacking", true);
        }

        if (Base.isEngaged == false)
        {
            anim.SetBool("IsAttacking", false);
            anim.SetBool("IsWalking", false);
        }
    }
}
