﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacingAnimationScript : MonoBehaviour
{
    public FaceTarget faceEnemy;
    public AttackTowerBase attack;
    Animator anim;
    public SpriteRenderer Renderer;
    // Start is called before the first frame update
    void Start()
    {
        //transform.Rotate(0f, 180f, 0f);
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attack.isAttacking == true)
        {
            anim.SetBool("IsAttacking", true);
        }

        if (attack.isAttacking == false)
        {
            anim.SetBool("IsAttacking", false);
        }

        if (faceEnemy.facingDir == FaceTarget.FacingDir.Right)
        {
            anim.SetBool("IsFacingRight", true);
            anim.SetBool("IsFacingLeft", false);
            anim.SetBool("IsFacingForward", false);
            anim.SetBool("IsFacingBackward", false);
            Renderer.flipX = true; // Flips the sprite via Sprite Renderer
        }

        if (faceEnemy.facingDir == FaceTarget.FacingDir.Left)
        {
            anim.SetBool("IsFacingLeft", true);
            anim.SetBool("IsFacingRight", false);
            anim.SetBool("IsFacingForward", false);
            anim.SetBool("IsFacingBackward", false);
            Renderer.flipX = false;
        }

        if (faceEnemy.facingDir == FaceTarget.FacingDir.Forward)
        {
            anim.SetBool("IsFacingForward", true);
            anim.SetBool("IsFacingRight", false);
            anim.SetBool("IsFacingLeft", false);
            anim.SetBool("IsFacingBackward", false);
        }

        if (faceEnemy.facingDir == FaceTarget.FacingDir.Backward)
        {
            anim.SetBool("IsFacingBackward", true);
            anim.SetBool("IsFacingRight", false);
            anim.SetBool("IsFacingLeft", false);
            anim.SetBool("IsFacingForward", false);
        }
    }
}
